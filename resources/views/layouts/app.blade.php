<!DOCTYPE html>
<html lang="en">
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>KMSE</title>
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    {{--<link rel="stylesheet" media="screen" href="{{ elixir('css/app.css') }}">--}}
    <link rel="stylesheet" media="screen" href="{{ elixir('css/style.css') }}">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <!-- Scripts -->
    {{--<script type="text/javascript" src="{{ elixir('js/app.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('js/all.js') }}"></script>
</head>
<body>
<div class="container-fluid">
    {{--@include('partials.navbar')--}}
    {{--@include('partials.slideshow')--}}
    @include('partials.header')
    @include('partials.navbar')
    @yield('content')
    @include('partials.footer')
</div>
<!-- Scripts -->

</body>
</html>
