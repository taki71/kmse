@extends('layouts.app')

@section('content')
    <h3>My Google Maps Demo</h3>
    <div id="map" style="width: 100%; height: 500px;"></div>
    <script>
        function initMap() {
            var uluru = {lat: 47.5176619, lng: 19.1806744};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnWEif7hnOnc5zWYNqk6RP9fWa_3_BNfA&callback=initMap">
    </script>
    <div class="container">


        {{--@if(session('message'))--}}
            {{--<div class='alert alert-success'>--}}
                {{--{{ session('message') }}--}}
            {{--</div>--}}
        {{--@endif--}}


        {{--<div class="row">--}}

            <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h1 class="mb-2 text-center">Üzenet</h1>
                @if(!empty($success))
                    <div class='alert alert-success'>
                        {{$success}}
                    </div>
                @endif
                <form class="form-horizontal" method="POST" action="/kapcsolat">
                    {{ csrf_field() }}
                    <div class="form-group">
                            <label for="Name">Név: </label>
                            <input type="text" class="form-control" id="name" placeholder="pl. Kovács János" name="name" required>
                    </div>

                    <div class="form-group">
                        <label for="email">E-mail: </label>
                        <input type="text" class="form-control" id="email" placeholder="john@example.com" name="email" required>
                    </div>

                    <div class="form-group">
                        <label for="phone">Telefon: </label>
                        <input type="text" class="form-control" id="phone" placeholder="pl. +36301234567" name="phone" >
                    </div>

                    <div class="form-group">
                        <label for="message">Üzenet: </label>
                        <textarea type="text" class="form-control luna-message" id="message" placeholder="Írja ide üzenetét" name="message" required></textarea>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" value="Send">Küldés</button>
                    </div>
                </form>
            </div>
            <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 ">
                <address>
                Név: XXXX X XXX <br>
                E-mail: dsfgdfg@dsdfgfdg.hu <br>
                Telefon: +36201234567
                </address>
            </div>
        {{--</div>--}}


    </div> <!-- /container -->
@endsection