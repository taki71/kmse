<div class="header">
	<header id="header">

		<!-- logo -->
		<nav id="logo">

			<div class="row">
				<div class="col-3 col-md-3 ">
					<a href="/" title="Kézilabda MSE">
						<img src="{{ asset('images/logo_359_150.png') }}" alt="Kézilabda MSE logo">
					</a>
				</div>
				<div class="col-5 col-md-5 col-md-offset-1 sponsors">
					<a href="/" title="Kézilabda MSE">
						<img src="{{ asset('images/sponsors/mksz.jpg') }}" alt="Kézilabda MSE logo">
					</a>
					<a href="/" title="Kézilabda MSE">
						<img src="{{ asset('images/sponsors/logo.png') }}" alt="Kézilabda MSE logo">
					</a>
					<a href="/" title="Kézilabda MSE">
						<img src="{{ asset('images/sponsors/hummel.jpg') }}" alt="Kézilabda MSE logo">
					</a>
					<a href="/" title="Kézilabda MSE">
						<img src="{{ asset('images/sponsors/xviker_logo.jpg') }}" alt="Kézilabda MSE logo">
					</a>
				</div>
				<div class="col-3 col-md-3">
					{{--<a href="/" title="Kézilabda MSE">--}}
					{{--<img src="{{ asset('images/dress/4.jpg') }}" width="150" height="150" alt="Kézilabda MSE logo">--}}
					{{--</a>--}}
					{{--<a href="/" title="Kézilabda MSE">--}}
					{{--<img src="{{ asset('images/dress/1.jpg') }}" width="150" height="150" alt="Kézilabda MSE logo">--}}
					{{--</a>--}}
					<a href="/" title="Kézilabda MSE">
					<img src="{{ asset('images/dress/2.jpg') }}" width="150" height="150" alt="Kézilabda MSE logo">
					</a>
					<a href="/" title="Kézilabda MSE">
					<img src="{{ asset('images/dress/3.jpg') }}" width="150" height="150" alt="Kézilabda MSE logo">
					</a>
				</div>
			</div>

			{{--<a href="/" title="Kézilabda MSE">--}}
				{{--<img src="{{ asset('images/logo.jpg') }}" alt="Kézilabda MSE logo">--}}
			{{--</a>--}}
			{{--<a href="/" title="Kézilabda MSE">--}}
				{{--<img src="{{ asset('images/logo.jpg') }}" alt="Kézilabda MSE logo">--}}
			{{--</a>--}}
			{{--<a href="/" title="Kézilabda MSE">--}}
				{{--<img src="{{ asset('images/logo.jpg') }}" alt="Kézilabda MSE logo">--}}
			{{--</a>--}}
			{{--<a href="/" title="Kézilabda MSE">--}}
				{{--<img src="{{ asset('images/logo.jpg') }}" alt="Kézilabda MSE logo">--}}
			{{--</a>--}}
		</nav>

	</header>
</div>