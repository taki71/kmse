{{--<ul class="nav navbar-nav">--}}
	{{--@foreach($menus as $item)--}}
		{{--<li class="active"><a href="{{ url($item['url']) }}">{{ $item['title'] }}</a></li>--}}
	{{--@endforeach--}}
{{--</ul>--}}

{{--<nav class="navbar navbar-inverse navbar-static-top">--}}
<div class="navbar">
<nav class="navbar-default">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			{{--<a class="navbar-brand" href="#"><img src="{{ asset('images/logo_359_150.png') }}" alt="Kézilabda MSE logo"></a>--}}
			<a class="navbar-brand" href="#">KMSE</a>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="{{ (Request::is('/') ? 'active' : '') }}">
					<a href="{{ url('') }}"><i class="fa fa-home"></i> Nyitó</a>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle {{ (Request::is('klubtortenet') ? 'active' : '') }}" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Egyesület<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('klubtortenet') }}">Klubtörténet</a></li>
						<li><a href="{{ url('hires-sportoloink') }}">Híres sportolóink</a></li>
						<li><a href="{{ url('tamogatok') }}">Támogatók és partnerek</a></li>
						<li><a href="{{ url('strandkezilabda') }}">Strandkézilabda</a></li>
						<li><a href="{{ url('sportorvos') }}">Sportorvos</a></li>
						<li><a href="{{ url('dokumentumok') }}">Letölthető anyagok</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle {{ (Request::is('utanpotlas-csapatok') ? 'active' : '') }}" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Utánpótlás<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('utanpotlas-csapatok') }}">Csapatok</a></li>
						<li><a href="{{ url('utanpotlas-eredmenyek') }}">Eredmenyek</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle {{ (Request::is('esemenynaptar') ? 'active' : '') }}" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Események<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('esemenynaptar') }}">Eseménynaptár</a></li>
						<li><a href="{{ url('eredmények') }}">Eredmények</a></li>
					</ul>
				</li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle {{ (Request::is('kepek') || Request::is('videok') ? 'active' : '') }}" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Galériák <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('2017-2018-szezon') }}">2017-2018 szezon</a></li>
                    </ul>
                </li>
				<li class="{{ (Request::is('kapcsolat') ? 'active' : '') }}">
					<a href="{{ url('kapcsolat') }}">Kapcsolat</a>
				</li>
				<li class="{{ (Request::is('webshop') ? 'active' : '') }}">
					<a href="{{ url('webshop') }}">Webshop</a>
				</li>

				{{--<li class="dropdown">--}}
					{{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>--}}
					{{--<ul class="dropdown-menu">--}}
						{{--<li><a href="#">Action</a></li>--}}
						{{--<li><a href="#">Another action</a></li>--}}
						{{--<li><a href="#">Something else here</a></li>--}}
						{{--<li role="separator" class="divider"></li>--}}
						{{--<li class="dropdown-header">Nav header</li>--}}
						{{--<li><a href="#">Separated link</a></li>--}}
						{{--<li><a href="#">One more separated link</a></li>--}}
					{{--</ul>--}}
				{{--</li>--}}
			</ul>

			<!-- Right Side Of Navbar -->
			<ul class="nav navbar-nav navbar-right">
				{{--<!-- Authentication Links -->--}}
				{{--@guest--}}
					{{--<li><a href="{{ route('login') }}">Login</a></li>--}}
					{{--<li><a href="{{ route('register') }}">Register</a></li>--}}
				{{--@else--}}
					{{--<li class="dropdown">--}}
						{{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">--}}
							{{--{{ Auth::user()->name }} <span class="caret"></span>--}}
						{{--</a>--}}

						{{--<ul class="dropdown-menu">--}}
							{{--<li>--}}
								{{--<a href="{{ route('logout') }}"--}}
								   {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();">--}}
									{{--Logout--}}
								{{--</a>--}}

								{{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
									{{--{{ csrf_field() }}--}}
								{{--</form>--}}
							{{--</li>--}}
						{{--</ul>--}}
					{{--</li>--}}
				{{--@endguest--}}
			{{--</ul>--}}


				<li class="social-links">
					<a href="https://www.facebook.com/kezilabdamse.hu"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>
				</li>
				<li class="social-links">
					<a href=""><i class="fa fa-envelope fa-lg"></i></a>
				</li>


			{{--<ul class="nav navbar-nav navbar-right">--}}
				{{--@if (Auth::guest())--}}
					{{--<li class="{{ (Request::is('auth/login') ? 'active' : '') }}"><a href="{{ url('auth/login') }}"><i--}}
									{{--class="fa fa-sign-in"></i> Login</a></li>--}}
					{{--<li class="{{ (Request::is('auth/register') ? 'active' : '') }}"><a--}}
								{{--href="{{ url('auth/register') }}">Register</a></li>--}}
				{{--@else--}}
					{{--<li class="dropdown">--}}
						{{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"--}}
						   {{--aria-expanded="false"><i class="fa fa-user"></i> {{ Auth::user()->name }} <i--}}
									{{--class="fa fa-caret-down"></i></a>--}}
						{{--<ul class="dropdown-menu" role="menu">--}}
							{{--@if(Auth::check())--}}
								{{--@if(Auth::user()->admin==1)--}}
									{{--<li>--}}
										{{--<a href="{{ url('admin/dashboard') }}"><i class="fa fa-tachometer"></i> Admin Dashboard</a>--}}
									{{--</li>--}}
								{{--@endif--}}
								{{--<li role="presentation" class="divider"></li>--}}
							{{--@endif--}}
							{{--<li>--}}
								{{--<a href="{{ url('auth/logout') }}"><i class="fa fa-sign-out"></i> Logout</a>--}}
							{{--</li>--}}
						{{--</ul>--}}
					{{--</li>--}}
				{{--@endif--}}
			{{--</ul>--}}
		</div>
	</div>
</nav>
</div>