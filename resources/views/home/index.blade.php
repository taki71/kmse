@extends('layouts.app')

@section('content')
    @include('partials.slideshow')
    @include('partials.marketing')
    @include('partials.article-list')
@endsection