@extends('layouts.app')

@section('content')<>
A STRANDKÉZILABDÁZÁS SZABÁLYAI
1. szabály: játéktér
1:1       A játéktér - amely egy mezőnyből és két kapuelőtérből áll - 27 m hosszú és 12 m széles, négyszögletes terület.

Felszínét legalább 40 centiméter mélységű homokréteg borítja.

A játéktér állapotát nem szabad egyik csapat előnyére sem megváltoztatni a mérkőzés ideje alatt.

A játékteret legalább 3 méter szélességű szabad terület övezi biztonsági zónaként.

1:2       A mezőny 15 méter hosszú és 12 méter széles. A határoló vonalakat legfeljebb 8 centiméter szélességű, rugalmas szalaggal kell megjelölni.

1:3       A játéktér minden vonala ahhoz a területhez tartozik, amelyet határol.

A játéktér hosszanti oldalait oldalvonalnak, rövidebb oldalait alapvonalnak hívjuk. A kapufák közötti vonal a gólvonal, amelyet meg kell jelölni.

A kapu:
1:4       Mindkét alapvonal közepén áll egy kapu. A kapukat biztonságosan kell a talajhoz rögzíteni. A kapuk belmérete: 2 m magas és 3 m széles. A kapukat a homokba szilárdan kell rögzíteni és a rögzítéseket biztonsági okokból védelemmel kell ellátni.

A két kapufát a keresztgerenda köti össze. A hátsó oldaluk az alapvonal hátsó szélével egy vonalba esik. A kapufák és a keresztgerenda négyzet keresztmetszetűek, oldalaik 8 centiméteresek.



A kapufák és a keresztgerenda mezőnyből látható három oldalát a háttértől lényegesen elütő két különböző színnel kell befesteni

A kapukat hálóval kell ellátni. A hálót úgy kell felerősíteni, hogy a kapura dobott labda normális körülmények között a kapuban maradjon.



A kapuelőtér

1:5       A kapuk előtt található a kapuelőtér (6. szabály). A kapuelőteret határoló kapuelőtérvonalat – szalaggal – az alapvonallal párhuzamosan attól 6 m távolságban  kell kijelölni.



A versenybírói asztal

1:6       A versenybírói asztalt az oldalvonal közepén, attól legalább 3 méter távolságban kell elhelyezni.

A versenybírói asztalt úgy kell elhelyezni, hogy a versenybírók rálássanak a cserehelyekre.



Cserehely

1:7       A mezőnyjátékosok részére a cserehely 15 méter hosszú és megközelítően 3 méter széles, az oldalvonalon kívül, a játéktér mindkét oldalán

1:8     a) A kapusnak a játékteret a cserehely oldalvonalán vagy annak meghosszabbításán a  saját kapuelőtérhez tartozó oldalvonalon át kell elhagyni. (4:13, 5:12)

b) A kapus a cserehelyének oldalán a saját kapuelőteréhez tartozó oldalvonalon keresztül léphet be a játéktérre.



2. szabály: A JÁTÉKIDŐ, A ZÁRÓJELZÉS



A játék indítása

2:1       A mérkőzés előtt a játékvezetők érme feldobásával döntik el a térfél- és a cserehely választást.

A sorsolást nyerő csapat választja meg vagy a játéktérfelet vagy a cserehelyet. A félidőben a csapatok térfelet cserélnek, azonban cserehelyet nem.

2:2       Mindkét félidő és az „Arany Gól”-ért való küzdelem játékvezetői labdával (10:1-2), a játékvezetők sípjelére kezdődik (2:5).

2:3       A játékosok elhelyezkedése tetszőleges a mezőnyben.



Játékidő

2:4       A mérkőzés két félidőből áll, mindkét félidő eredményét önállóan kell értékelni. Mindkét félidő 10 perc (2:6, 2:8 és 4:2 szabályok), közöttük legfeljebb 5 perc szünettel.

2:5       A játékidő (a játékvezető labda elvégzése és az óra indítása)  játékvezetői sípjellel kezdődik.

2:6       Amennyiben a félidő végén döntetlen az eredmény, az „arany-gól” szabályt (9:7) kell alkalmazni, vagyis a következő gól dönti el azt, hogy ki nyeri a félidőt. A játékot ebben az esetben játékvezetői labdával (10. szabály) kell újrakezdeni. Minden félidő győztese 1 pontot kap.

2:7       Abban az esetben, ha mindkét félidőt ugyanazon csapat nyeri meg, akkor 2:0 végeredménnyel ő a mérkőzés győztese.

2:8       Akkor, ha mindkét csapat nyer egy-egy félidőt az eredmény döntetlen. Azonban mindig kell győztest hirdetni, ezért ebben az esetben a „szétlövést” (9. szabály) kell alkalmazni.





A zárójelzés

2:9       A játékidő a nyilvános időmérő berendezés vagy az időmérő záró-jelzésével ér véget. Ha ilyen jelzés nem hangzik el, a játékvezető sípol és jelzi, hogy a játékidő letelt (17:10, 18:1, 18:2).

Megjegyzés:

Amennyiben automatikus zárójelezéssel működő nyilvános időmérő berendezés nem áll rendelkezésre, az időmérő asztali vagy kézi stopperórát vesz igénybe és zárójelzéssel fejezi be a mérkőzést (18:2).

Nyilvános időmérő berendezés használatakor, a nézők jobb tájékoztatása érdekében az óra 10-től 0-ig vagy 0-10-ig jelezhet.

2:10     Azokat a szabálytalanságokat és sportszerűtlenségeket, amelyek a zárójelzés elhangzása előtt vagy azzal egy időben (a félidő vagy a mérkőzés vége) fordultak elő, még a jelzés elhangzása után is büntetni kell. A játékvezetők a még végrehajtandó szabaddobás vagy 6 m-es dobás eredményének megvárása után fejezik be a mérkőzést.

2:11     Amennyiben a szabaddobás vagy a büntetődobás végrehajtása alatt, illetve amikor a kapura dobott labda a levegőben van, elhangzik a zárójelzés, a dobást meg kell ismételni. A dobás eredményét meg kell várni, a játékvezetők csak azután fejezhetik be a mérkőzést.

2:12     A 2:10-11. szabálypontok alapján végrehajtott szabaddobás vagy 6 m-es dobás közben, játékosok és hivatalos személyek elleni szabálytalanságok vagy sportszerűtlen magatartás elkövetéséért személyes büntetést kell alkalmazni. Az ilyen dobások végrehajtásakor elkövetett szabálytalanság miatt azonban az ellenfél nem juthat szabaddobáshoz.

2:13     Ha a játékvezetők megállapítják, hogy az időmérő túl korán jelezte a félidő vagy a mérkőzés végét, akkor kötelesek a játékosokat a játéktéren tartani és a játékidő különbözetet utána játszatni.

A játék folytatásakor a labda annál a csapatnál marad, amelyiknél a korai zárójelzés időpontjában volt. Ha a labda nem volt játékban, a mérkőzés a játékhelyzetnek megfelelő dobással folytatódik, különben szabaddobással a 13:4 a-b szabály szerint.

Amennyiben az első félidőt (vagy a meghosszabbítást) később jelezték, a második félidőt a megfelelő idővel le kell rövidíteni. Ha a mérkőzés második félidejét (vagy a meghosszabbítást) jelezték később, a játékvezető már nem tud a helyzeten változtatni.





Time-out (játékidő megszakítás)



2:14     A játékvezetők döntik el, hogy mikor és mennyi időre kell a játékidőt megszakítani.

A következő helyzetekben kötelező a játékidő megszakítása:

Kizárás, végleges kiállítás

6 m-es dobás

Holtidő (team time-out)

Az időmérő vagy technikai küldött (az időmérő-titkári asztalnál helyet foglaló szövetségi képviselő) jelzése

A játékvezetők között szükséges egyeztetés a 17:9 szerint

Egy hivatalos személy büntetése.



Amennyiben a körülmények azt kívánják, más helyzetekben is megszakítható a játékidő.

Néhány olyan eset, amikor ugyan nem kötelező a time-out, azonban normális körülmények között adható:

a játékos sérülésekor,

a csapat nyilvánvaló időhúzásakor, (ha a csapat késlelteti  a dobás elvégzését, ha a játékos eldobja a labdát vagy nem engedi el, vagy ha a játékos túl lassan hagyja el a játékteret egy büntetés után),

szabálytalan játékoscserénél vagy „többlet" játékos játéktérre lépése esetén

külső körülmények miatt (pl. ha a szalagot újból rögzíteni kell, mert elmozdult).

2:15     A játékidő megszakítás alatt elkövetett szabálytalanságok ugyanolyan következményekkel járnak, mintha azokat a játékidőben követték volna el (16:16).

2:16     Játékidő megszakításkor a játékvezetők jelt adnak az időmérőnek az óra megállítására.

A játékidő megszakításának jelzése: három rövid sípjel és a kezekkel alakított „T" (16. kézjelzés).

A játékidő megszakítása után a játék folytatásakor sípolni kell (15:3 b). Az időmérő ezzel a sípjellel indítja az órát.





Holtidő (team time-out)



2:17     Mindkét mérkőző csapat a rendes játékidőben félidőnként egy perc időtartamú holtidőt (team time-out) igényelhet.

A csapat egyik hivatalos személye az erre célra rendszeresített „zöld lap” egyértelmű felmutatásával jelzi az időkérést. Az oldalvonal közepére megy és feltartja a lapot jól látható módon, hogy az időmérő azonnal észre vegye. (A zöld lap ajánlatos mérete: 30x20 cm és mindkét oldalán egy nagy T betű áll.)

A csapat csak akkor kérhet holtidőt, ha a labdát birtokolja (ha a labda játékban van, vagy a játék megszakadt). A holtidőt azonnal ki kell adni. Ha a csapat elveszíti a labdát, mielőtt az időmérő a sípjelzést megadta volna, a holtidőt nem lehet kiadni.

A holtidő kérésekor az időmérő sípjellel megszakítja a játékot, kézjelzést ad (16. sz. jelzés) és nyújtott karral a holtidőt kérő csapat irányába mutat. A zöld lapot az oldalvonal közepének felénél, azon kívül kb. 1 méterre a homokba láthatóan kell elhelyeznie a hivatalos személynek és a félidő végéig ott is kell hagynia.

A játékvezető megszakítja a játékidőt és az időmérő megállítja az órát. A játékvezető megerősíti a holtidő tényét, az időmérő pedig egy külön órát indít el a holtidő ellenőrzéséhez. A titkár bejegyzi a jegyzőkönyvbe a holtidőt és azt, hogy melyik félidőben és melyik csapat részéről volt az időkérés.

A holtidő alatt a játékosok és a hivatalos személyek a saját cserehelyük környezetében vagy a játéktéren vagy a cserehelyen tartózkodhatnak. A játékvezetők a mezőny közepén állnak, egyikük azonban rövid időre a versenybírói asztalhoz megy egyeztetés céljából.

A holtidőben elkövetett vétségeket úgy kell elbírálni, mintha azokat a játékidő alatt követték volna el. Annak itt nincs jelentősége, hogy a játékosok a játéktéren vagy azon kívül helyezkednek el. A 8:4, 16:1d és a 16:2c pont szerinti időleges kiállítás adható sportszerűtlen magatartásért.

50 másodperc elteltével az időmérő sípol, jelezve azt, hogy a játékot 10 másodperc múlva folytatni kell.

A holtidő lejártakor a csapatok kötelesek a játék folytatására készek lenni.

A játék a holtidő megadásakor fennállott játékhelyzetnek megfelelő dobással folytatódik vagy ha a labda játékban volt, akkor a holtidőt kérő csapat szabaddobásával onnan, ahol a labda a játékidő megszakításkor volt.

Az időmérő a játékvezető sípjelére indítja az órát.

Megjegyzés:

A labda birtoklását jelenti az is, ha a játékot kidobással, bedobással, szabaddobással vagy büntetődobással kell folytatni.

A labda játékban van, ha a játékos a kezében tartja, dobja, elfogja vagy társának átadja, illetve a csapat birtokolja.







3. szabály: A labda



3:1       A mérkőzéseket gömbölyű, nem csúszós gumilabdával játsszák. A férfiak labdamérete:  súlya 350-370 gramm, kerülete 54-56 cm közötti; a női labda 280-300 gramm súlyú és 50-52 cm kerületű. A gyermekmérkőzéseken kisebb labda is használható.

3:2       Minden mérkőzésre legalább 3 labdát kell biztosítani. Egy-egy tartalék labdát közvetlenül a két kapu mögött, egy arra kijelölt helyen kell elhelyezni.

3:3       A játék- és játékidőmegszakítás csökkentése érdekében, a játékvezető által kijelölt kapusnak minél hamarabb játékba kell hoznia a labdát, ha az elhagyta a játékteret.





4. szabály: A csapat, játékoscsere, felszerelés



A csapat

4:1 A strandkézilabda mérkőzéseket játszhatják női, férfi és vegyes csapatok.

4:2       Egy csapat maximum 8 játékosból áll, amelyből a mérkőzés kezdetekor legalább 6 játékosnak kell jelen lennie. Ha a játékra alkalmas játékosok száma 4 alá csökken, a játékot félbe kell szakítani és a kérdéses csapat elvesztette a mérkőzést.

4:3       A játéktéren csapatonként 4 játékos (3 mezőny és a kapus) tartózkodhat. A többiek cserejátékosok, akik a saját cserehelyükön tartózkodhatnak.

4:4       A játékos vagy a hivatalos személy akkor rendelkezik részvételi jogosultsággal, ha a kezdésnél jelen van és neve a versenyjegyzőkönyvben szerepel.

A mérkőzés megkezdése után érkező játékosoknak és hivatalos személyeknek az időmérőtől, a titkártól kell részvételi jogosultságot kapniuk és nevüket be kell írni a versenyjegyzőkönyvbe.

A részvételi jogosultsággal rendelkező játékos saját cserehelyénél bármikor beléphet a játéktérre (lásd azonban a 4:13. szabályt).

Ha egy részvételi jogosultsággal nem rendelkező játékos a játéktérre lép, kizárással kell büntetni (16:6 a). A mérkőzés az ellenfél javára ítélt szabaddobással folytatódik (13:1 a-b szabály).

4:5       A csapatnak az egész mérkőzés alatt a játéktéren egy játékost kapusként kell szerepeltetni. Az a játékos, akit kapusnak jelöltek, bármikor szerepelhet mezőnyjátékosként. A mezőnyjátékos ugyancsak bármikor átveheti a kapus feladatkörét (figyelembe véve a 4:8 szabályt).

4:6       A csapat a mérkőzés folyamán legfeljebb 4 hivatalos személyt foglalkoztathat. A hivatalos személyek a mérkőzés alatt nem cserélhetők. Közülük egy személyt csapatfelelősként kell megjelölni. Csak ő jogosult az időmérővel, a titkárral és esetleg a játékvezetővel tárgyalni (kivéve a 2:17 szabályt).

A hivatalos személyek a mérkőzés alatt általában nem jogosultak a játéktérre lépni. A szabály elleni vétséget mint sportszerűtlen magatartást kell büntetni (8:4,16:1d, 16:2d és 16:6 b). A játékot az ellenfél javára ítélt szabaddobással kell folytatni (13:1 a-b és figyelemmel a 8. magyarázatra).

4:7       Sérülés esetén a játékvezetők engedélyt adhatnak a játéktérre való belépésre játékidő megszakítás után (16 és 17. karjelzéssel egyidejűleg) két - részvételre jogosult - személynek (ld. 4:4), hogy a csapat sérült játékosának segítséget nyújtson (16:2).





Felszerelés



4:8       A csapat mezőnyjátékosainak egységes öltözéket kell viselniük, amelynek színe, kombinációja és megjelenése az ellenféltől világosan megkülönböztethető legyen.

A kapusként szerepeltetett játékosok öltözékének színe ugyancsak különbözzön a játékostársak és az ellenfél csapatának (mezőnyjátékosok és kapus) öltözéke színétől. Maximum két játékos jelölhető kapusként (17:3).

Megjegyzés:

A kapusként szereplő mezőnyjátékosnak olyan megkülönböztető mezt kell viselnie, amelyen keresztül jól látható eredeti számozása is.

4:9       A játékosok kötelesek elöl legalább 10 cm nagyságú számozást viselni.

A számok színe jól megkülönböztethető kell, hogy legyen az öltözék színétől. Megengedett, hogy a számozást a felkarra vagy a combra tegyék a játékos testi épségének veszélyeztetése nélkül.

4:10     Minden játékos mezítláb játszik. Megengedett zokni vagy sport (egészségügyi) kötés viselése, azonban cipő vagy bármilyen más lábbeli viselése nem engedélyezett.

4:11     A játékosokat veszélyeztető tárgyak nem viselhetők. Ilyenek pl. fej- és arcvédők, karkötők, karórák, gyűrűk, láncok, fülbevalók, keret és rögzítés nélküli szemüvegek, valamint minden más olyan tárgy, amelyek a játékosokat veszélyeztetik (17:3). Homlokszalag viselhető, amennyiben puha, rugalmas anyagból készült. Napellenzős sapka megengedett, ha a kemény előrészét hátrafordítják a sérülések elkerülése végett.

Azok a játékosok, akik ezeket az előírásokat nem teljesítik, a szabálytalanság megszüntetéséig nem játszhatnak

Megjegyzés:

Orrvédő: A szabály szerint nem megengedett fejvédő vagy arcmaszk viselése. Ezt úgy kell értelmezni, hogy a maszk befedi az egész arcot, az orrvédő mérete sokkal kisebb és csak az orrot fedi be. Ezek figyelembevételével az orrvédő viselhető.

4:12     Ha egy játékos vérzik, illetve a teste vagy az öltözéke véres, azonnal és felszólítás nélkül el kell hagynia a játékteret a cserehelynél a vérzés megállítása, a seb letakarása és a vér lemosása (a testéről és a felszerelésről) céljából. Amíg ez nem történik meg, a játékos nem térhet vissza a játéktérre.

Azt a játékost, aki ebben a tekintetben nem követi a játékvezető utasításait, sportszerűtlenség miatt büntetni kell (8:4, 16:1 d és 16:2 c).





Játékos-csere

4:13     A cserejátékosok a mérkőzés alatt az időmérőnél vagy a titkárnál való jelentkezés nélkül bármikor és ismételten cserélhetők, amennyiben a lecserélt játékosok elhagyták a játékteret (16:2 a).

A játékteret elhagyni és oda belépni csak a saját cserehelyen szabad (16:2 a). Ez a kapuscserére is érvényes (5:12).

A játékoscsere szabályai érvényesek a játékidő megszakítás - time out - alatt is (a holtidő kivétel)

Szabálytalan csere esetén a játék az ellenfél szabaddobásával (13. szabály) vagy 6 m-es dobással (14. szabály) folytatódik, ha a játékot meg kell szakítani. Más esetekben a játék a játékhelyzetnek megfelelő dobással folytatódik. A szabálytalan cserét a vétkes játékos időleges kiállításával kell büntetni (16. szabály). Amennyiben egy adott helyzetben ugyanabból a csapatból többen is szabálytalanul cserélnek, akkor csak azt a játékost kell büntetni, aki először szabálytalankodott

4:14     Ha egy játékos (az alaplétszámon felül), többletjátékosként csere nélkül lép a játéktérre, vagy egy játékos a cserehelyről jogosulatlanul játékba avatkozik, időlegesen ki kell állítani és csapatából egy másik játékosnak is el kell hagyni a játékteret.

Ha az időlegesen kiállított játékos a kiállítási idő alatt a játéktérre lép, újabb kiállítást kell kapnia, ami egyúttal a kizárását jelenti és ezen felül egy másik játékosnak is el kell hagynia a játékteret.

Mindkét esetben a mérkőzés az ellenfél javára ítélt szabaddobással folytatódik (13:1 a-b. szabály).



5. szabály: A kapus



A kapusnak megengedett:

5:1       A kapus a kapuelőtérben védekezés céljából a labdát bármely testrészével érintheti.

5:2       A kapus a kapuelőtérben korlátlanul mozoghat a labdával - figyelmen kívül hagyva a mezőnyjátékosokra érvényes előírásokat (7:2-4, 7:7). A kidobás végrehajtását azonban nem halogathatja (6:5,12:2 és 15:3 b).

5:3       A kapus labda nélkül elhagyhatja a kapuelőteret és részt vehet a mezőnyjátékban. Ebben az esetben a mezőnyben játszó játékosokra érvényes szabályok vonatkoznak rá.

A kapuelőtér elhagyásának számít, ha a kapus bármely testrészével a kapuelőtérvonalon kívül érinti a talajt.

5:4       A kapus a labda teljes értékű birtoklása nélkül elhagyhatja a kapuelőteret, és a labdát a mezőnyben továbbjátszhatja.

A kapusnak nem megengedett:

5:5       A kapus védésnél nem veszélyeztetheti az ellenfelet (8:2, 8:5).

5:6       A kapus a birtokba vett labdával nem hagyhatja el a kapuelőteret (szabaddobás 13:1a szerint, ha a játékvezető sípolt a kidobás elvégzésére; máskülönben a kidobást meg kell ismételni).

5:7       A kapus kidobás után a kapuelőtéren kívül csak akkor érintheti újra a labdát, ha azt egy másik játékos érintette (13:1 a).

5:8       A kapus mindaddig nem érintheti a kapuelőtéren kívül a talajon fekvő vagy guruló labdát, amíg a kapuelőtérben tartózkodik (13:1 a).

5:9       A kapus nem viheti vissza a kapuelőtérbe a kapuelőtéren kívül a talajon fekvő vagy guruló labdát (13:1 a).

5:10     A kapus a labdával nem térhet vissza a mezőnyből saját kapuelőterébe (13:1 a).

5:11     A kapus lábbal (térdkalács alatt) nem érintheti a mezőny irányába mozgó vagy a kapuelőtérben fekvő labdát (13:1 a).



A kapus cseréje

5:12     A kapus csak a saját cserehelye felöl, kapuelőterének oldalvonalán át léphet a játéktérre (1:8, 4:13).

A kapus a saját cserehelyének oldalvonalán át (1:8, 4:13) hagyhatja el a játékteret.





6. szabály: a kapuelőtér

6:1       A kapuelőtérben csak a kapus tartózkodhat (lásd azonban 6:3). Belépésnek tekintjük azt, ha a kapuelőteret, beleértve a kapuelőtérvonalat egy mezőnyjátékos valamely testrészével megérinti.

6:2       A mezőnyjátékos kapuelőtérbe lépését a következők szerint kell büntetni:

szabaddobás, ha a mezőnyjátékos labdával lép a kapuelőtérbe (13:1 a),

szabaddobás, ha a mezőnyjátékos labda nélkül lép a kapuelőtérbe és ezáltal előnyhöz jut (13:1 a-b, lásd azonban 6:2 c),

büntetődobás, ha a védőjátékos a kapuelőtérbe való belépése által egy tiszta gólhelyzetet megakadályoz (14:1 a).

6:3       A kapuelőtérbe lépés büntetlen marad:

ha a játékos a labda eldobása után lép a kapuelőtérbe úgy, hogy az az ellenfélnek nem hátrányos,

ha a játékos labda nélkül lép a kapuelőtérbe és ezáltal nem szerez előnyt,

ha a védőjátékos a védekezési kísérletnél vagy azután lép a kapuelőtérbe úgy, hogy az ellenfélnek nem hátrányos.

6:4       A kapuelőtérben levő labda a kapusé (ld. azonban 6:5).

6:5       A kapuelőtérben fekvő, guruló labdát a mezőnyjátékos megjátszhatja anélkül, hogy testének bármely részével a kapuelőteret érintette volna. (szabaddobás).

A kapuelőtér felett levegőben levő labda megjátszható, kivéve kidobáskor (12:2).

6:6       Ha a labda a kapuelőtérbe jut, azt a kapusnak kidobással ismét játékba kell hozni (12. szabály).

6:7       Ha a védőcsapat játékosa védekezési kísérletnél megérinti a labdát, amelyet azután a kapus elfog, vagy a labda a kapuelőtérben marad, a játék kidobással folytatódik (6:6).

6:8       Ha a labdát a saját kapuelőtérbe játsszák, a következők szerint kell ítélni:

gól, ha a labda a kapuba kerül;

szabaddobás, ha a labda a kapuelőtérben marad, vagy ha a kapus megérinti és az nem jut a kapuba (13:1a-b):

bedobás, ha a labda az alapvonalon túlra jut - a kapun kívül (12:1);

a játék tovább folytatódik, ha a labda keresztül halad a kapuelőtéren és a mezőnybe jut anélkül, hogy a kapus megérintette volna.

6:9       A kapuelőtérből a mezőnybe visszakerülő labda játékban marad.





7. szabály: Játék a labdával, passzív játék



Játék a labdával

Megengedett:

7:1       A labdát szabad kézzel, karral, fejjel, törzzsel, combbal és térddel elfogni, megállítani, dobni, lökni vagy ütni. Lehet vetődni is a talajon fekvő vagy guruló labdára.

7:2       A labdát legfeljebb 3 másodpercig szabad érinteni, akkor is, ha az a talajon van (13:1 a).

A labda nem érintheti a talajt 3 másodpercnél tovább úgy, hogy azt ugyanaz a játékos vegye fel, aki utoljára érintette azt (szabaddobás).

7:3       Az elfogott labdával legfeljebb 3 lépést szabad mozogni (13:1 a).

Egy lépésnek tekintjük:

ha mindkét lábával a talajon álló játékos az egyik lábát felemeli és ismét visszahelyezi, vagy az egyik lábát egyik helyzetből egy másikba mozgatja;

ha a játékos a talajt egyik lábával érinti és a labdát elfogja, s ezután a másik lábával megérinti a talajt;

ha a játékos egy ugrás után egyik lábával érinti a talajt és utána ugyanazzal a lábával ugrik, vagy a talajt a másik lábával megérinti;

ha a játékos egy ugrás után mindkét lábával egyidejűleg érinti a talajt és utána az egyik lábát felemeli és ismét visszahelyezi, vagy az egyik lábát egyik helyzetből egy másikba mozgatja.

Megjegyzés:

Ha a láb az egyik helyzetből egy másikba mozog, szabad a másik lábat utána húzni, ami összesen csak egy lépés.

7:4       Szabad a labdát mind helyben, mind futás közben:

egyszer a talajra dobni és egy vagy két kézzel ismét elfogni;

ismételten egy kézzel a talajra ütni (egykezes labdavezetés) vagy a labdát egy kézzel a talajon ismételten gurítani és azután egy vagy két kézzel ismét elfogni, illetve felvenni.

Mihelyt azonban a labdát egy vagy két kézzel ismét elfogják, legfeljebb 3 lépés, illetve 3 másodperc után tovább kell játszani (13:1 a).

A labda vezetése (talajra dobása vagy ütögetése) csak akkor kezdődik, ha a játékos valamely testrészével megérinti a labdát és azt a talajra irányítja.

Ha a labda egy másik játékost, a kapufákat vagy a keresztgerendát érintette, szabad újból vezetni és ismét elfogni.

7:5       Szabad a labdát az egyik kézből a másikba átvezetni.

7:6       Szabad a labdát térdelve, ülve vagy fekve továbbjátszani.

Nem megengedett:

7:7       Nem szabad a labdát egynél többször megérinteni anélkül, hogy az közben a talajt, egy másik játékost, a kapufákat vagy a keresztgerendát érintette volna (13:1 a).

A fogáshibák büntetlenül maradnak.

Megjegyzés:

Fogáshiba, ha a labda elfogásának vagy megállításának kísérleténél nem sikerül megszerezni (elfogni, birtokba venni) a labdát.

A már megszerzett labdát csak egy labdavezetési folyamat után szabad ismételten megérinteni (tehát, ha a labda a talajt érintette).

7:8       Nem szabad a labdát lábbal (térdkalács alatt) érinteni kivéve, ha azt az ellenfél dobta a játékosra (13:1 a-b).

7:9       Ha a labda a játéktéren levő játékvezetőt érinti, a játék folytatódik.

Passzív játék

7:10     Nem szabad a labdát a saját csapat birtokában tartani a támadási akciók felismerhetősége nélkül, illetve a kapura lövési kísérletek elmulasztásával. Ez passzív játék, amiért a játékvezetőnek a labdát birtokló csapat ellen szabaddobást kell ítélni (13:1 a).

A szabaddobást azon a helyen kell végrehajtani, ahol a labda a játékmegszakításkor volt.

7:11     Ha a passzív játékra utaló tendencia felismerhető, a játékvezetőnek figyelmeztető jelzést (18. kézjelzés) kell alkalmazni. Ez a labdát birtokló csapatnak alkalmat ad arra, hogy támadásvezetését átállítsa és így a labda elvételét elkerülje. Amennyiben a figyelmeztető jelzés után sem változik a támadási modor, vagy nem lőnek kapura, a labdát birtokló csapat ellen meg kell ítélni a szabaddobást.

Rendkívüli helyzetekben (pl. a tiszta gólhelyzet szándékos kihagyása) a játékvezető előzetes figyelmeztető jelzés nélkül is ítélhet szabaddobást a labdát birtokló csapat ellen.




8. szabály: MAGATARTÁS AZ ELLENFÉLLEL SZEMBEN



Megengedett:

8:1 a karokat és a kezeket a labda sáncolásához vagy birtoklásához használni,

a labdát az ellenféltől nyitott kézzel minden irányból kijátszani,

a labda nélküli vagy a labdát birtokló ellenfelet testtel elzárni,

elölről, hajlított karokkal az ellenféllel testi kapcsolatot felvenni, őt ellenőrizni és kísérni.

Nem megengedett:

8:2 az ellenfél birtokában levő labdát kiszakítani vagy kiütni,

az ellenfelet karral, kézzel vagy lábbal elzárni vagy eltaszítani,

az ellenfelet átkarolni, megfogni, lökni, az ellenfélnek rohanni vagy nekiugrani,

az ellenfelet más módon (labdával vagy anélkül) szabálytalanul zavarni, akadályozni vagy veszélyeztetni.

8:3       Azokért a 8:2-ben felsorolt szabálytalanságokért, amelyek túlnyomórészt vagy kizárólag az ellenféllel szemben és nem a labdáért irányulnak, progresszív büntetést kell alkalmazni.

A progresszív büntetés azt jelenti, hogy azokat az ellenféllel szembeni vétségeket, amelyek a labdáért való küzdelemben előforduló szokásos és elfogadható szabálytalanság ismérveit túlhaladják, nem elég csak szabaddobással vagy büntetődobással büntetni.

Mindazon szabálytalanságokért, amelyek kimerítik a progresszív büntetés feltételeit, személyes büntetést kell alkalmazni (16:1 b, 3 b és 6 g).

8:4       Azok a testi és szóbeli kifejezésmódok, amelyek nem egyeztethetők össze a sportszerűség szellemével, sportszerűtlen magatartásnak minősülnek. Ez érvényes a játékosokra és a hivatalos személyekre a játéktéren és azon kívül is. Sportszerűtlen magatartás esetén progresszív büntetést kell alkalmazni (16:1d, 16:2c-d,16:6b,h,i).

8:5       Azt a játékost, aki az ellenfelet az egészségre veszélyesen megtámadja, kizárással kell büntetni (16:6 c), különösen akkor, ha a dobóhelyzetben levő játékos dobókarját oldalról vagy hátulról megüti vagy visszahúzza;

cselekményével az ellenfél fejét vagy nyakát találja el;

lábbal, térddel vagy más módon szándékosan az ellenfél testét támadja meg, beleértve a buktatást;

futásban vagy ugrásban levő ellenfelet lök vagy úgy támad meg, hogy azáltal elveszíti a teste feletti ellenőrzést;

közvetlen szabaddobáskor a kapura dobott labda eltalálja a védőjátékos fejét, illetve büntetődobáskor a kapus fejét azzal a feltétellel, hogy a védőjátékos, illetve a kapus nem végzett helyváltoztatást vagy szélességben nem mozdult el.

8:6       Játékos vagy hivatalos személy játéktéren vagy azon kívül elkövetett durva sportszerűtlen magatartását kizárással kell büntetni (16:6 e).

8:7       A játékidő alatt elkövetett tettlegességért a vétkes játékost véglegesen ki kell állítani (16:11-13). A játékidőn kívüli tettlegesség esetén kizárást kell alkalmazni (16: 6f, 16:16 b és d). Azt a hivatalos személyt, aki tettlegességet követ el, ki kell zárni (16:6 g).

Megjegyzés:

A tettlegesség a hatályos szabály értelmében különösen erős és szándékos támadás más személy ellen (játékos, játékvezető, időmérő-titkár, hivatalos személy, szövetségi képviselő, néző stb.). Másként fogalmazva: több mint egyszerű reflexcselekvés és több mint felelőtlen vagy túlzott módszer a védekezési kísérletkor. A köpés tettlegességnek számít.

8:8       A 8:2-7-ben felsorakoztatott vétségek esetén 6 m-es büntetődobást kell ítélni (14:1), ha az közvetlenül vagy közvetve a tiszta gólhelyzet megakadályozását okozza.

Egyébként a szabálytalanságért szabaddobást kell ítélni (13:1 a-b, azonban ld. 13:2 és 13:3).





9. szabály: gól és a mérkőzés győztesének eldöntése



9:1    Gólt érnek el, ha a labda teljes terjedelmében túljutott a gólvonalon, amennyiben a dobás előtt vagy alatt sem a dobó, sem játékostársai nem követtek el szabálytalanságot.
Ha a labda a kapuba jut, jóllehet a védekező csapat játékosa szabálytalanságot követett el, gólt kell ítélni.
Ha a játékvezető vagy az időmérő a játékot megszakította, mielőtt a labda túljutott a gólvonalon, nem szabad gólt ítélni.
Ha a játékos a labdát a saját kapujába játssza, az ellenfél javára kell gólt ítélni (öngól), kivéve, amikor a kapus kidobást hajt végre (12:2 második bekezdés).





Megjegyzés:

Ha a labda kapuba jutását a mérkőzésen illetéktelen tárgy vagy személy megakadályozza, gólt kell ítélni, ha a játékvezető meggyőződik arról, hogy

a labda a beavatkozás nélkül gólba jutott volna. (Ebben az esetben természetesen érvénytelen az a feltétel, „ha a labda teljes terjedelmével túljutott a gólvonalon".)

9:2    A látványos gólokat (légpasszal, testfordulattal vagy test mögötti kapura lövéssel) két ponttal kell jutalmazni.

9:3    A 6 m-es dobásból elért gól 2 pontot ér.

9:4    Gólt követően a játék kidobással folytatódik. (12:1)

9:5    Ha a játékvezető gólt ítélt és ezt követően a kapus elvégezte a kapus kidobást, a gólt már nem szabad visszavonni.
Ha a gól és a kapus kidobása között elhangzik a zárójelzés (a félidő és a mérkőzés végén), akkor ezt a gólt a játékvezetőknek kidobás nélkül egyértelműen el kell ismerni.




Kapus által elért gól


9:6    A kapus által szerzett gól két pontot ér.
A mérkőzés győztesének eldöntése

9:7    Ha valamelyik félidő végén döntetlen az állás az ún. „aranygól” szabályt kell alkalmazni, vagyis az lesz a félidő győztese, aki ezt követően az első gól dobja (2:6).

9:8    Ha mindkét csapat megnyert egy-egy félidőt, az ún. „szétlövést” (egy játékos a kapus ellen) szabályt kell alkalmazni.
Öt-öt játékra jogosult mindkét csapatból felváltva hajtja végre a dobásokat.
Ha a kapus is a dobók között van, a dobás végrehajtása közben mezőnyjátékosnak számít (tehát nem ér kettőt a dobott gólja).
Az a győztes, aki több gólt dob az öt dobásból. Ha az öt dobás után sincs döntés a végeredményt illetően, a „szétlövés” folytatódik.
Ehhez először térfelet kell cserélnie a csapatoknak (anélkül, hogy a cserehelyet megcserélték volna- ld. megjegyzést).
Újra csapatonként öt-öt játékra jogosult felváltva hajtja végre a dobásokat. Most a másik csapat kezdi a dobásokat.
A második vagy még későbbi „körökben” a dobások csak addig folytatódnak, amíg valamelyik csapat azonos dobásszám után előnyre tesz szert.

Megjegyzés: a mérkőzés végeredményének eldöntése „szétlövéssel”

A „szétlövésnél” is pénzfeldobással döntik el a játékvezetők, hogy ki választhat térfelet és ki a dobások megkezdését. Ha az egyik csapat a sorsolást megnyeri és a kezdést választja, akkor az ellenfélnek joga van a térfelet megválasztania. Vagy éppen fordítva, ha az egyik csapat a sorsolást megnyerve a térfelet választja, akkor az ellenfél joga a „szétlövés” kezdése.


Mindkét kapus legalább az egyik lábával a gólvonalon áll. A dobást végző mezőnyjátékos a kapuelőtérvonal és az oldalvonal valamelyik találkozásánál áll. A játékvezető sípja után a dobó játékos visszajátssza a labdát a gólvonalon álló kapusának. A passz közben a labda nem érintheti a talajt. Mihelyt a dobó játékos kezét elhagyta a labda a kapus előre mozoghat. A kapusnak a kapuelőtérben kell maradnia. Három másodpercen belül kell a kapusnak vagy az ellenfél kapujába dobnia a labdát, vagy indítania a közben az ellenfél kapuja felé futó saját játékosát. Az átadás közben sem érintheti a labda a talajt.

A mezőnyjátékosnak el kell fognia a labdát, és szabályos körülmények között gólt kell elérni.

Ha a támadó csapat kapusa vagy dobó játékosa szabálytalanságot követ el, akkor a támadás véget ér.

Ha a védekező csapat kapusa elhagyja a kapuelőteret, oda bármikor szabadon visszatérhet.

Ha a játékosok száma 5 alá csökken, akkor az érintett csapatnak eggyel kevesebb lehetősége lesz a szétlövésnél, mert egy játékos nem dobhat másodszor is.

9:9    Ha a védekező csapat kapusa úgy hárít egy „szétlövést”, hogy közben szabálytalanságot követ el, akkor büntetődobást kell ítélni (16:6 d).

Megjegyzés:

Bármelyik játékra jogosult játékos végrehajthatja a büntetődobást.

9:10    A „szétlövés” alatt minden mezőnyjátékosnak a cserehelyén kell tartózkodnia. Amelyik játékos elvégezte a dobást vissza kell térnie a cserezónába.

10. szabály: a játékvezetői labda

10:1    Mindkét félidő és az „aranygólért” folytatott játék játékvezetői labdával kezdődik (2:2).

10:2    A játékvezetői labdát a mezőny közepén az egyik játékvezető hajtja végre a labda függőleges feldobásával, miután a másik játékvezető megadta erre a sípjelet.

10:3    A másik játékvezető az időmérői asztallal szemben az oldalvonalon kívül helyezkedik el.

10:4    Mindkét csapatból egy-egy játékost kivéve, a többi játékos szabadon helyezkedhet el a játékvezetői labda végrehajtása alatt a játéktéren úgy, hogy legalább 3 méterre legyenek a játékvezetőtől. A két labdáért ugró játékosnak a saját kapujuk felöl, a játékvezető mellett kell állniuk.


10:5    A labda akkor játszható meg, miután az elérte a legmagasabb pontot.

11. szabály: a bedobás

11:1    Bedobással kell játékba hozni a labdát, ha az teljes egészében áthaladt az oldalvonalon, vagy utoljára a védekező csapat játékosát érintve, a saját alapvonalon túlra jutott.

11:2    A bedobást sípjel nélkül (lásd azonban 15:3 b) az a csapat hajtja végre, amelynek játékosai nem érintették utoljára a labdát, mielőtt az túljutott az oldalvonalon vagy az alapvonalon (a kaput kivéve).

11:3    A bedobást azon a helyen kell végrehajtani, ahol a labda elhagyta az oldalvonalat. Amennyiben a labda a kapuelőtéren keresztül hagyta el a játékteret (a kaput kivéve), akkor a bedobást a kapuelőtérvonaltól legalább 1 méterre, a mezőny irányába kell végrehajtani.

11:4    A bedobást végző játékosnak egyik lábával az oldalvonalon kell állni, amíg kezét a labda nem hagyta el. A labdát ugyanannak a játékosnak nem szabad letenni és ismét felvenni vagy vezetni és ismét elfogni (13:1 a).

11:5    Bedobásnál az ellenfél játékosai nem léphetnek 1 méternél közelebb a dobó játékoshoz.

12. szabály: a kidobás

12:1    Kidobással kell játékba hozni a labdát,

ha az ellenfél gólt ért el,
ha az a kapuelőtérbe jut - a kapus birtokolja vagy a talajon marad (6:6),
ha az alapvonalon túlra kerül, amennyiben utoljára a kapust vagy az ellenfél egyik játékosát érintette.

Ez azt jelenti, hogy az adott helyzetben a labda játékon kívül van.

Amennyiben a kapus csapatának bármely tagja a kidobás elvégzése előtt szabálytalanságot követ el, a játék a kidobás szabályszerű elvégzésével folytatódik (lásd 13:3)

12:2    A kidobást sípjel nélkül a kapuelőtérből a kapuelőtér-vonalon keresztül kell végrehajtani (kivéve 15:3 b).

A kidobást akkor kell végrehajtottnak tekinteni, ha a kapus által eldobott labda áthaladt a kapuelőtér-vonalon. A másik csapat játékosai jóllehet a kapuelőtér-vonalnál tartózkodhatnak, a labdát csak akkor érinthetik, ha az áthaladt a kapuelőtér-vonalon (15:7, 3. bekezdés).

12:3    A kapus a kidobás végrehajtása után a labdát csak akkor érintheti ismét, miután az egy másik játékost érintett (5:7 13:1 a).

13. szabály: a szabaddobás

A szabaddobás megítélése

13:1    A játékvezetők alapvetően a következő esetekben szakítják meg a játékot és ítélnek szabaddobást a vétlen csapat javára: a labdát birtokló - lényegében a támadócsapat - szabálytalanságot  követ el, s emiatt nem tarthatja meg a labdát (4:4, 4:6, 4:13, 4:14, 5:6-11, 6:2 a-b, 6:4, 6:8b, 7:2-4, 7:7-8, 7:10, 8:8, 11:4, 12:3, 13:9, 14:5-7 és 15:2-5),
a védekező csapat szabálytalansága miatt a támadócsapat elveszíti a labdát (4:4, 4:6, 4:13, 4:14, 6:2 b, 6:4, 6:8 b, 7:8, 8:8, 13:7).

13:2    A játékvezetőknek biztosítaniuk kell a játék folyamatosságát azzal, hogy elhamarkodott szabaddobás ítélettel nem szakítják meg a játékot.

Ez azt jelenti, hogy a játékvezetők a 13:1 a pontban felsoroltak esetén nem ítélnek szabaddobást, ha a védekező csapat közvetlenül a támadócsapat által elkövetett szabálytalanság után labdához jut.

Hasonlóképpen a 13:1 b pont szabálytalanságai esetén csak akkor avatkoznak a játékba, ha a támadócsapat az ellenfél szabálytalansága miatt elveszíti a labdát vagy nincs olyan helyzetben, hogy a támadást folytassa.

Ha szabálytalanság miatt személyes büntetést kell alkalmazni, a játékvezetők azonnal megszakíthatják a játékot, ha az a vétlen csapat számára - amelyik nem követte el a szabálytalanságot -, nem jelent hátrányt. Egyébként a büntetés az akció befejezéséig elhalasztható.

A 13:2 nem érvényes a 4:3, 4:4, 4:5, 4:6, 4:13, 4:14 szabályok elleni vétségek esetében, amelyeknél az időmérő jelzésével a játékot azonnal meg kell szakítani.

13:3    Ha a 13:1 pontban felsorolt szabálytalanságok egyikét - amelyért egyébként szabaddobást kell ítélni - akkor követik el, amikor a labda nincs játékban, a mérkőzést a játékmegszakítás okának megfelelő dobással kell folytatni.

13:4    Kiegészítve a 13:1 a-b pontokat, bizonyos esetekben, amikor a játék szabálytalanság nélkül szakad meg (a labda játékban volt), a mérkőzést szabaddobással kell folytatni:

ha a megszakításkor a labdát az egyik csapat birtokolta és a labdát a továbbiakban is megtartja,
ha a labdát az egyik csapat sem birtokolta, az a csapat tartja meg a labdát, amelyiknél a megszakítást megelőzően volt, ha a játék azért szakad meg, mert a labda a játéktér feletti tárgyat érintett, akkor a játékot az a csapat folytatja, amelyik nem érintette utoljára a labdát.

13:5    A labdát birtokló csapat elleni szabaddobás ítéletnél, ha a labda még ennek a csapatnak az egyik játékosánál van, azt azon a helyen, ahol éppen van, azonnal el kell engedni, vagy a talajra tenni (16:2 e).

A szabaddobás végrehajtása

13:6    A szabaddobás végrehajtásakor a támadó csapat játékosainak 1 méternél távolabb kell tartózkodniuk az ellenfél kapuelőtér-vonalától.

13:7    A szabaddobás végrehajtásakor az ellenfél játékosainak 1 méternél távolabb kell tartózkodniuk a dobó játékostól.

13:8    A szabaddobást a játékvezető sípjele nélkül elvileg azon a helyen kell végrehajtani, ahol a szabálytalanságot elkövették.

Kivételt az alábbiak képeznek:

A 13:4 a-b pontokban leírt helyzetekben a szabaddobást ott kell végrehajtani, ahol a labda a megszakítás időpontjában volt. A 13:4 c pontban megfogalmazott esetben a szabaddobást - szintén a játékvezető sípjele után - a játéktérnek azon a részén kell végrehajtani, amely megfelel a labda és a mennyezet (egyéb tárgy) találkozási helyének.
Ha a játékvezető vagy a szövetségi képviselő a védekező csapat játékosának vagy hivatalos személyének szabálysértése miatt megszakítja a játékot és szóbeli figyelmeztetés vagy személyes büntetés következik, a szabaddobást azon a helyen kell végrehajtani, ahol a labda a megszakításkor volt, amennyiben ez a hely a vétlen csapat javára kedvezőbb mint a vétség helye.
Ez a kivétel azonos azzal a helyzettel, amikor az időmérő szabálytalan csere vagy járulékos belépés miatt (4:3, 4:4, 4:6, 4:13, 4:14) megszakítja a játékot.
Ugyancsak a korábban már tisztázottak szerint passzív játék esetén (7:10) a szabaddobást azon a helyen kell végrehajtani, ahol a labda a játékmegszakításkor volt.
A szabaddobást - bármely eddig felsorolt esetben - sohasem szabad a dobásra jogosult csapat kapuelőterében végrehajtani. Amennyiben a végrehajtási pozíció az előbb említett behatárolt területre esne, a dobást azon kívül a legközelebbi helyen kell végrehajtani

Ha a szabálytalanság a védekező csapat kapuelőtér-vonalához képest közelebb mint 1 méteren belül történt, a szabaddobást a kapuelőtér-vonaltól legalább 1 méter távolságban kell végrehajtani.

13:9    Amennyiben a szabaddobásra jogosult csapat játékosa labdával a kézben, a dobás végrehajtására megfelelő helyen tartózkodik, a labdát nem szabad letennie és ismét felvennie, vagy vezetnie (13:1 a).

14. szabály: büntetődobás

A büntetődobás megítélése

14:1    büntetődobást kell ítélni:

az egész játéktéren a tiszta gólhelyzet megakadályozásáért az ellenfél játékosa vagy hivatalos személye által,
a tiszta gólhelyzetben elhangzott jogosulatlan sípjel esetén,
a tiszta gólhelyzet megakadályozásáért a játékban nem illetékes személy beavatkozása által (kivéve: 9:1 megjegyzése).

14:2    Ha a támadócsapat játékosa a szabálytalanság ellenére (14:1 a) uralja a labda és a test feletti helyzetet, nem szabad büntetődobást ítélni akkor sem, ha a játékos utána elvéti a gólhelyzetet.

Mindig, amikor alkalmasint büntetődobás ítélhető, a játékvezető csak akkor avatkozzon közbe, ha világosan megállapítja, hogy az ítélet tényszerűen jogos és szükséges. Ha a támadójátékos az ellenfél indokolatlan beavatkozása ellenére kapura lövést kezdeményez, nincs ok a büntetődobásra. Amennyiben világosan felismerhető, hogy a játékos a szabálytalanság miatt valóságosan elveszítette a labda és a teste feletti ellenőrzést, s ezáltal a tiszta gólhelyzet már nem áll fenn, büntetődobást kell ítélni

14:3    A büntetődobás megítélésekor a játékvezetőnek játékidő megszakítást kell elrendelni (2:8).

14:4    A büntetődobásból elért gól két pontot ér (9:3).

A büntetődobás végrehajtása

14:5    A büntetődobást a mezőnyjátékvezető sípjele után 3 másodpercen belül, mint kapura lövést kell végrehajtani (13:1 a).

14:6    A büntetődobás végrehajtásakor a dobójátékos a kapuelőtérvonalat nem érintheti és nem lépheti át, amíg a labda a kezét nem hagyta el (13:1 a).

14:7    A büntetődobás végrehajtása után a dobójátékosnak vagy játékostársának csak akkor szabad a labdát újra érintenie, miután az ellenfél játékosát, a kapufákat vagy a keresztgerendát érintette (13:1 a).

14:8    A büntetődobás végrehajtásakor az ellenfél mezőnyjátékosainak és kapusának legalább 1 m-re kell tartózkodniuk a dobó játékostól, amíg a labda a dobó kezét nem hagyta el. Egyébként a büntetődobást meg kell ismételni, ha a labda nem jutott gólba.

14:9    Amennyiben a támadójátékos labdával a kézben korrekt dobóhelyzetet foglalt el és kész a büntetődobás végrehajtására, a kapuscsere már nem engedélyezhető. Ha a kapuscserét mégis megkísérlik, sportszerűtlen magatartásként kell büntetni (8:4,16:1d, 16:2c).

15. szabály: A DOBÁSOK VÉGREHAJTÁSA

15:1    A dobás végrehajtása előtt a labdának a dobójátékos kezében kell lenni.
Minden játékosnak az illető dobásnak megfelelő helyzetet kell elfoglalni. Valamennyi játékosnak a korrekt pozícióban kell maradni mindaddig, amíg a labda a dobójátékos kezét nem hagyta el.
A hibás felállást helyesbíteni kell (ld. azonban 15:7).

15:2    A kidobás kivételével a dobások végrehajtásakor a dobójátékos egyik lába valamely részének  megszakítás nélkül a talajon kell maradni (13:1 a). A másik lábat szabad a talajról felemelni és ismét letenni.

15:3     A játékvezetőnek sípolni kell

minden büntetődobásnál;
a bedobás, a kidobás, és a szabaddobás alábbi eseteiben:
time-out után a játék folytatásakor,
a játék folytatásakor szabaddobás esetén a 13:4 szerint,
a dobások végrehajtásának késleltetésekor,
a játékosok elhelyezkedésének helyesbítése után,
a szóbeli figyelmeztetés után

A sípjelet követően a dobójátékosnak a labdát 3 másodpercen belül játékba kell hozni (13:1 a).

15:4    A dobást akkor kell végrehajtottnak tekinteni, amikor a labda a játékos kezét elhagyta (kivéve 12:2).
A dobás végrehajtásánál a dobójátékos nem adhatja át a labdát játékostársának, illetve az nem érintheti a kezében levő labdát (13:1 a).

15:5    A dobójátékos csak akkor érintheti ismét a labdát, miután az egy másik játékost, a kapufákat vagy a keresztgerendát érintette (13:1 a).

15:6    Valamennyi dobásból közvetlen gól érhető el, kivéve a kidobást, amelyiknél öngól nem lehetséges (12:2), és a játékvezetői labdát (mert azt a játékvezető hajtja végre).

15:7    A kezdődobás, a bedobás vagy szabaddobás végrehajtásakor az ellenfél szabálytalan felállását a játékvezetőnek nem kell helyesbíteni akkor, ha az azonnali végrehajtás a dobó csapatnak nem jelent hátrányt. A szabálytalan elhelyezkedést helyesbíteni kell, ha az a dobó csapat számára hátrányos helyzetet teremt (15:3 b).

Ha a játékvezető az ellenfél hibás felállása ellenére sípol, úgy a hibás felállásban levő játékosok teljesen akcióképesek.

Ha az ellenfél túl közel állással vagy egyéb szabálytalansággal késlelteti vagy befolyásolja a dobás végrehajtását, először szóban figyelmeztetni kell, ismétlődés esetén időlegesen ki kell állítani.

16. szabály: büntetések

Időleges kiállítás

16:1    Időleges kiállítás adható:

Az ellenféllel szembeni magatartásban elkövetett szabálytalanságokért (5:5, 8:2), amelyek nem tartoznak a 8:3 szerinti progresszív büntetés kategóriájába.
A progresszív büntetéssel járó szabálytalanságokért (8:3).
A formális dobások végrehajtásakor az elleniét által elkövetett szabálytalanságokért (15:7).
A játékos vagy hivatalos személy sportszerűtlen magatartásáért (8:4).

16:2    Időleges kiállítást kell alkalmazni:

Hibás cseréért vagy járulékos belépésért (4:13, 4:14).
Az ellenféllel szembeni magatartásban ismételten elkövetett, progresszivitással büntetendő szabálytalanságokért (8:3).
A játéktéren levő - vagy azon kívül tartózkodó - játékos ismételten elkövetett sportszerűtlen magatartásáért (8:4).
A hivatalos személy ismételten elkövetett sportszerűtlen magatartásáért.
Ha a labdát birtokló csapat elleni szabaddobás ítéletkor nem bocsátják szabadon vagy nem teszik le a labdát (13:5).
A formális dobások végrehajtásakor a másik csapat által ismételten elkövetett szabálytalanság esetén (15:7).
Játékos vagy hivatalos személy játékidő alatti kizárásáért (16:8 második bekezdés).
Ha az időlegesen kiállított játékos, még a játék folytatása előtt sportszerűtlen magatartást tanúsít (16:6 h).

16:3    A játékvezetőnek világosan kell jelezni az időleges kiállítást a vétkes játékos és az időmérő számára (14. kézjelzés).

16:4    Az időlegesen kiállított játékos a kiállítási idő alatt nem vehet részt a játékban és csapata a játéktéren nem egészülhet ki.

A kiállítási idő a játék folytatására adott sípjelzéskor kezdődik.

A kiállított játékos akkor léphet ismét a játéktérre vagy akkor helyettesíthető másik játékossal, amikor a támadás-védekezés pozícióváltás megtörtént (lásd még a 16. szabály 2. megjegyzését)

16:5    Ugyanazon játékos második kiállítása kizárást eredményez.

A második időleges kiállítást követő kizárás is mindig a játékidő hátralevő részére szól (16. szabály 3. megjegyzés), az ilyen kizárásról nem kell jelentést készíteni.

Kizárás

16:6    Kizárást kell alkalmazni:

Részvételi jogosultsággal nem rendelkező játékos belépéséért (4:4).
A hivatalos személy második sportszerűtlen magatartásáért, miután a csapat valamelyik hivatalos személye a 16:2 d pont alapján már kapott időleges kiállítást (8:4).
Azokért a szabálytalanságokért, amelyek veszélyeztetik az ellenfél játékosának testi épségét (8:5).
Azokért a kapus által elkövetett szabálytalanságokért, amelyek a „szétlövés” alatt a kapuelőteret elhagyva, az ellenfél egészségét veszélyeztetve követ el a kapus (8:5: nem a labdára irányuló, egyértelműen az ellenfél testét célzó akciók).
Játékos vagy hivatalos személy játéktéren vagy azon kívül elkövetett durva, sportszerűtlen magatartásáért (8:6).
Játékos játékidőn kívül (a mérkőzés megkezdése előtt vagy a félidei szünetben) elkövetett tettlegességéért (8:7, 16:16 b,d).
Hivatalos személy tettlegességéért (8:7).
Ugyanannak a játékosnak második időleges kiállításáért (16:5).
Játékos vagy hivatalos személy által a félidei szünetben ismételten elkövetett sportszerűtlen magatartásáért (16:16 d).

16:7    A játékvezetőnek time-out után a „piros lap" magasba tartásával világosan kell jelezni a kizárást a vétkes játékos, illetve a hivatalos személy (időmérő, titkár) számára (13. kézjelzés, a „piros lap" nagysága 12x9 cm nagyságú „kártya").

16:8    A játékos vagy a hivatalos személy kizárása mindig a játékidő hátralevő részére szól és azonnal el kell hagyniuk mind a játékteret, mind a cserehelyet. Távozásuk után a csapattal semmilyen formában nem tarthatnak kapcsolatot.

A játékos vagy a hivatalos személy játékidő alatti - a játéktéren vagy azon kívül - kizárását, a csapat számára mindig időleges kiállítással kell egybekötni. Ez azt jelenti, hogy a csapat játékosainak számát a játéktéren egy fővel csökkenteni kell (kivéve 16:6 b). A kizárt játékos akkor helyettesíthető másik játékossal, mihelyt támadás-védekezés pozícióváltás megtörtént (ld. még a 16. szabály 2. megjegyzését).

16:9    A kizárást (kivéve a második időleges kiállításért adottat - 16:6 h.) fel kell tüntetni a versenyjegyzőkönyvben (jelentést kell készíteni róla az MKSZ Strandkézilabda Bizottsága számára)

16:10     Ha a kapus vagy a mezőnyjátékos a „szétlövés” alatt sportszerűtlen vagy durva sportszerűtlen szabálytalanságot követ el, kizárást kell alkalmazni.

16:11 A "Szétlövésnél" ha a védő kapus elállja a támadó játékos útját, fizikai kapcsolatba kerül vele, akkor büntetődobást és kizárást kell ítélni. A védő kapus viseli minden esetben az ilyen jellegű akciók felelősségét.


Végleges kiállítás

16:12  Végleges kiállítást kell alkalmazni:
a játékidő alatt - a játéktéren kívül is - elkövetett tettlegességért (a 8:7 definíciója szerint).

16:13      A végleges kiállítást játékidő megszakítás után a vétkes játékossal, az időmérővel és a titkárral a 15. kézjelzéssel - a karok keresztezése a fej fölött - világosan közölni kell.

16:14  A végleges kiállítás mindig a játékidő hátralevő részéig szól és a csapat a továbbiakban a játéktéren eggyel kevesebb játékossal játszhat.

A véglegesen kiállított játékos nem pótolható és azonnal el kell hagynia a játékteret, valamint a cserehelyet, s a továbbiakban a csapattal semmilyen formában nem tarthat kapcsolatot.

16:15 A végleges kiállítás tényét a játékvezetőknek a versenyjegyzőkönyvben az MKSZ Strandkézilabda Bizottságának jelenteni kell.

Több vétség ugyanabban a szituációban

16:16     Amennyiben a játékos vagy a hivatalos személy egyidejűleg vagy közvetlen folyamatban, mielőtt a játékvezető a mérkőzés folytatására sípjelzést adott volna több szabálytalanságot követ el, amelyek különböző büntetést igényelnek, alapvetően csak a legsúlyosabb büntetést kell kiróni. Ez különösen akkor érvényes, ha a vétség tettlegesség volt.

Szabálytalanságok a játékidőn kívül

A verseny színhelyén, a játékidőn kívül a játékos vagy a hivatalos személy által elkövetett sportszerűtlen magatartást, durva sportszerűtlen magatartást, vagy tettlegességet, a következők szerint kell büntetni:

A mérkőzés előtt:

sportszerűtlen magatartásért szóbeli figyelmeztetés (16:1 d),
durva sportszerűtlen magatartásért vagy tettlegességért kizárás (16:6 e-g), amely után a csapat azonban 8 játékossal és 4 hivatalos személlyel kezdheti a mérkőzést.

A mérkőzés szünetében:

sportszerűtlen magatartásért szóbeli figyelmeztetés (16:1 d),
ismételt vagy durva sportszerűtlen magatartásért, valamint tettlegességért kizárás (16:6 b, e-g-i). További sportszerűtlen

magatartás esetén a 16:2 c-d szabályt is alkalmazni kell, ami egyébként a játékidő alatt érvényes.

A félidei szünetben alkalmazott kizárás után a csapat ugyanazzal a létszámmal folytathatja a játékot, mint a szünet előtt.

A mérkőzés után:

írásos jelentés

1. Megjegyzés: a játékidő

A 16:1,16:2,16:6 és a 16:11 szabálypontok általánosan összefoglalják a játékidő alatt előforduló szabálytalanságokat.
E tekintetben a játékidőhöz tartoznak a játékidő megszakítások, az ún. „aranygól” és a „szétlövés”, de a félidei szünet nem.

2. Megjegyzés: a labdabirtoklás változása/ támadásváltás

Labdabirtoklás váltás vagy támadásváltás akkor történik, ha a labda az egyik csapattól a másikhoz kerül.

Kivételek és magyarázatok:

A második félidő kezdetekor, az „aranygól” és a „szétlövés” esetén a kiállított játékos visszatérhet vagy helyettesíthető más játékossal.

Kiállított védőjátékos és büntetődobás kombinációja:

Ha a támadó csapat gólt ér el, a kiállított játékos visszatérhet vagy helyettesíthető más játékossal a kapus kidobás után.
Ha a támadó nem ér el gólt, a kiállított játékos vagy az ő cseréje a következő támadásváltásig várnia kell a játéktérre való belépéssel.

Ha késleltetett kiállítás alkalmaznak a játékvezetők előnyszabály alkalmazásakor:

A kiállítás akkor kezdődik, amikor azt kiadták, azaz az előnyszabályos akció, illetve az intézkedés (büntetés) megtörtént.

3. Megjegyzés:

A játékidő hátralévő része (16:5) magában foglalja az „aranygólt” és a „szétlövést” is.

17. szabály: a játékvezetők

17:1    A mérkőzést két egyenjogú játékvezető vezeti. Működésüket egy időmérő és egy titkár segíti.

17:2    A játékvezetők felügyelete a játékosok magatartása felett a sportlétesítményben való megjelenéskor kezdődik és annak elhagyásáig tart.

17:3    A játékvezetők felelősek a játéktér, a kapuk és a labdák állapotának mérkőzés előtti megvizsgálásáért. Ők állapítják meg, hogy melyik labdával játszanak (1. szabály és 3:1).

Ezen kívül a játékvezetők ellenőrzik, hogy mindkét csapat az előírás szerinti sportöltözékben jelen van-e, felülvizsgálják a versenyjegyzőkönyvet, a játékosok felszerelését.
Ők gondoskodnak arról, hogy a cserehely határain belül a játékosok és a hivatalos személyek száma megfelelő legyen. Megállapítják mindkét csapatfelelős jelenlétét és azonosságát. Minden pontatlanságot meg kell szüntetni (4:2-3 és 4:8-10).

17:4    A sorsolást a mérkőzés megkezdése előtt az egyik játékvezető a másik játékvezető és a két csapatkapitány jelenlétében végzi el (2:1).

17:5    Az egyik játékvezető a mérkőzés megkezdésekor az időmérői asztallal szemben az oldalvonalon kívül helyezkedik el. Az ő kezdődobáshoz adott sípjelével indul a játékidő (2:5).

A másik játékvezető a játéktér közepén helyezkedik el és a sípjel után ő indítja el a játékot a játékvezetői labdával.

A mérkőzés alatt a játékvezetők időnként térfelet cserélnek.

17:6     A játékvezetőknek úgy kell elhelyezkedniük, hogy mindkét cserehelyre rálátásuk legyen. (17:11, 18:1)

17:7    A mérkőzést elvileg ugyanannak a két játékvezetőnek kell végigvezetni.

A játékvezetők felelősek a mérkőzés játékszabályok szerinti folyamatáért, ezért minden szabálytalanságot büntetni kell (kivéve 13:2 és 14:2).

Ha a mérkőzés alatt az egyik játékvezető kiválik, a mérkőzést a másik játékvezető egyedül vezeti. (A világ és kontinens rendezvényeken a mindenkori szabályzat, illetve versenykiírás mérvadó.)

17:8    Ha szabálytalanságnál mindkét játékvezető ugyanazon csapat ellen sípol, de eltérően ítélkeznek, mindig a súlyosabb ítélet marad életben.

17:9

Ha a két játékvezető eltérően ítéli meg egy gól után, hogy az hány pontot ér, rövid egyeztetést követően a közösen kialakított döntés marad érvényben (ld. Megjegyzés).

Ha egy szabálytalanságnál mindkét játékvezető sípol, vagy a labda elhagyta a játékteret, de a játékvezetők ellentétesen jelzik a játék folytatásának irányát, rövid megbeszélés - egyeztetés - után a közösen kialakított döntés érvényes (ld. Megjegyzés).

Ekkor kötelező a time-out. A megbeszélés befejezése után a játékot egyértelmű kézjelzés után, sípjellel kell folytatni (2:8 f, 15:3 b).

Megjegyzés:

A játékvezetők rövid egyeztetést követően hozzák meg a döntést. Amennyiben nem sikerül közös álláspontot létrehozni a mezőny-játékvezető véleménye a mérvadó.

17:10 Mindkét játékvezető felelős a gólok számolásáért, a játékidő megtartásáért és a végeredményért. Ha az időmérés helyességét illetően kétségek adódnak, a játékvezetők egyeztetett döntése érvényes (lásd a 17:9 megjegyzését is).

17:11     Mindkét játékvezetőnek az időmérő/titkár segítségével ellenőriznie kell a cserejátékosok ki- és belépést (17:6, 18:1).

17:12 A játékvezetők felelősek a versenyjegyzőkönyv szabályszerű kitöltéséért.

A kizárásokat - ha azokat a 16:8 leírt vétségért alkalmazták -, és a végleges kiállításokat (16:14) a jelentésben meg kell indokolni.

17:13 A játékvezetők megfigyelései alapján hozott ítéletek és ténydöntések megtámadhatatlanok.

Csak a szabályokkal ellentétes döntéseikkel szemben emelhető kifogás.
A mérkőzés alatt a csak a mindenkori csapatfelelősök jogosultak a játékvezetőhöz szólni.

17:14     Mindkét játékvezető jogosult a játékot megszakítani vagy félbeszakítani.
A félbeszakítás előtt azonban a játék folytatása érdekében mindent meg kell kísérelni.

18. szabály: az időmérő és a titkár

18.1    Alapvetően az időmérő felelős a játékidő és a játékidő megszakítás hitelességéért. Szükség esetén kizárólag az időmérő jogosult a játékot megszakítani.

Hasonlóképpen a titkár felelős a játékos listáért, a versenyjegyzőkönyvért, a mérkőzés eredményének követéséért, a később érkező játékosok belépéséért és a részvételre nem jogosult játékosok játéktérre – nem - lépéséért.

A további feladatokért, így a cserehelyen tartózkodó játékosok és hivatalos személyek létszámáért közösen felelősek.

Mindkettőjüknek segíteni kell a játékvezetőket a játékoscsere ellenőrzésében.


Megjegyzés:

I.H.F rendezvényeken és a nemzeti bajnokságokban a fenti feladatok másképp is szervezhetőek.

18:2    Amennyiben nem áll rendelkezésre nyilvános időmérő berendezés, az időmérő mindkét csapatfelelőst tájékoztatja a lejátszott vagy a még hátralévő játékidőről, különösen játékidő megszakítás után.

Ha a nyilvános időmérő berendezés nem rendelkezik automatikus záró-jelzéssel, az időmérő felelős a félidő vagy a mérkőzés végén a zárójelzés megadásáért (lásd 2:9 megjegyzését).

Játékos kiállításánál a titkár egy lap felmutatásával erősíti meg a játékos  és a játékvezetők felé a kiállítást, amelyen „1” vagy  „2” jelenti azt, hogy az adott játékosnak ez az első vagy a második kiállítása.



Játékvezetői kÉZjelzések

Kézjelzések

Azt, hogy a gól hány pontot ér (9., 14:4 szabályok), a mezőny játékvezetőnek kell jeleznie egy vagy két ujjának mutatásával. Ha két pontot ért a gól, a kapu játékvezető karkörzéssel egészíti ki az előbbi jelzést.

Kézjelzés 14.: a játékvezető jelzi a durva szabálytalanságot és rámutat a vétkes játékosra. A behajlított kart csuklónál megragadó másik kar kiállítás jelez.

A játékvezetők piros lap felmutatásával jelzik a kizárást.

A titkárnak a piros lap felmutatásával meg kell erősítenie a kizárást.

Szabaddobáskor és bedobáskor a játékvezetőknek azonnal a dobás irányát kell jelezni (7. és 9. kézjelzés). Ezt követi - a játékhelyzet kényszerítése esetén - a vétség szerint kötelező személyes büntetés jelzése (13-15. kézjelzés). Ha hasznosnak tűnik a szabaddobás és a büntetődobás ítéletek a megfelelő jelzéssel indokolhatók tájékoztató jelleggel (1-6. és 11. kézjelzés). A 11. kézjelzést azonban csak akkor szükséges felmutatni, ha a passzív játék miatt megítélt szabaddobást megelőzően nem alkalmazták a 18. kézjelzést.

A 11-17. számú kézjelzések alkalmazása kötelező.

A 8., 10 és 18 jelzéseket csak akkor kell alkalmazni, ha a játékvezetők azt szükségesnek tartják.



A kézjelzések listája:

1. Belépés a kapuelőtérbe
2. Fogás - és labdavezetési hiba
3. Lépés- és időszabály megsértése
4. Átkarolás, megragadás, lökés
5. Ütés
6. Támadófault
7. Bedobás
8. Kidobás
9. Szabaddobás - irány
10. Az 1 m-es távolság figyelmen kívül hagyása
11. Az 1 pontos gól elismerése
12. A két pontos gól elismerése
13. Időleges kiállítás
14. Kizárás (piros)
15. Végleges kiállítás
16. Játékidő megszakítás
17. Engedély a játéktérre való belépéshez 2 főnek time-out alatt
18. Figyelmeztető jelzés a passzív játékra
@endsection
