<?php

namespace App\Http\Controllers;

use App\Notifications\InboxMessage;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Http\Requests\ContactFormRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Admin;

class ContactController extends Controller
{
    public function show()
    {
        return view('contact');
    }

    public function mailToAdmin(ContactFormRequest $message, Admin $admin)
    {        //send the admin an notification
        $admin->notify(new InboxMessage($message));
        // redirect the user back
        return redirect()->back()->with('message', 'Köszönjük üzenetét! Hamarosan vissza fogunk jelezni!');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'message' => 'required'
        ]);

        $input = $request->all();

        Contact::create($input);
        return view('contact')->with('success','Az üzenetét elmentettük.');
    }
}