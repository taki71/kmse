<?php

namespace App\Http\Requests;

use App\Http\Requests\ContactFormRequest;
use Illuminate\Foundation\Http\FormRequest;

class ContactFormRequest extends FormRequest
{
    protected $message;

    public function __construct(ContactFormRequest $message)
    {
        $this->message = $message;
    }

    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max: 255',
            'email' => 'required|email|max: 255',
            'message' => 'required',
        ];
    }

    /**
     * @param $notifiable
     * @return mixed
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(config('admin.name') . ", üzenet érkezett!")
            ->greeting(" ")
            ->salutation(" ")
            ->from($this->message->email, $this->message->name)
            ->line($this->message->message);
    }
}